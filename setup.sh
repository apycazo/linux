#!/bin/bash

# Install Ansible
sudo apt-get install software-properties-common -y
sudo apt-add-repository ppa:ansible/ansible -y
sudo apt-get update -y
sudo apt-get install ansible -y

# Install git
sudo apt-get install git -y

# Run playbook
sudo ansible-pull -U https://gitlab.com/apycazo/linux.git

printf "Done!\n"
