# Linux resources repository

## About

Repository for Linux resources, based around an Ansible playbook to setup an Ubuntu development machine quickly.

## Usage

Execute install and setup script with:
```bash
wget -O - https://gitlab.com/apycazo/linux/raw/master/setup.sh | bash
```

## Installs

Running the ansible playbook provided will update and upgrade the system, and install the following applications and packages:

* **git**: for ansible to read this repository by default.
* **vim**: command line text editor.
* **tmux**: terminal multiplexer.
* **build-essential**: basic development tools, required by some installations.
* **~~libgconf-2-4~~**: required to install Visual Studio Code cleanly. (not anymore).
* **chromium-browser**: open source chrome base browser.
* **filezilla**: file management tool.
* **pinta**: paint.net linux port for image edition.
* **openjdk-8-jdk**: java development kit.
* **maven**: java build management tool.
* **nvm**: nodejs version management and installation tool
* **nodejs**: latest version, installed from nvm.
* **docker**: container manager tool.
* **docker-compose**: extension to manage multiple-container deployments.
* **~~portainer~~**: docker gui interface, installed as a docker container.
* **IntelliJ IDEA community**: java IDE. (installed through snaps).
* **Visual Studio Code**: multi-language editor (javascript, python, typescript, css, html...) (installed through snaps)
* **Postman**: rest api testing tool (installed through snaps).

## To review

* Portainer can be accessed through http://localhost:9000, and will require to set user/password on the first access.
* To list the snap-installed applications use `snap list`.

## Bugs

* NVM is installed, but with root privileges on the $USER home (when run as root).
* Portainer has been removed due to a failure downloading the config file.

## Fixes

### WARNING : Failed to connect to lvmetad. Falling back to device scanning

- Edit `/etc/lvm/lvm.conf`, and set `use_lvmetad=0`.
- Update: `update-initramfs -k $(uname -r) -u; sync`.

## Tricks

### XFCE terminal drop-down

Using xfce, configure a keyboard shortcut at `Configuration -> Keyboard -> Shortcuts` 
and assign the order: `xfce4-terminal --drop-down`.

## References

* Ansible guide: https://opensource.com/article/18/3/manage-workstation-ansible
* Free icons: https://icons8.com/free-icons
* Royalty-free icons: https://icons8.com/icon/set/royalty-free
* Free technology icons: https://icons8.com/free-icons/technology

## Pending list

* gradle
* nginx
* tomcat
* apache
* jenkins
* mysql


